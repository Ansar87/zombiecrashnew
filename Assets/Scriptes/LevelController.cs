﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LevelController :MonoBehaviour
{

    [Header("Префабы")]
    [SerializeField] GameObject PrefabGamer;
    [SerializeField] GameObject PrefabEnemy;
    [SerializeField] GameObject PrefabAlly;
    [SerializeField] GameObject EnemyAndResourcesParent;

    [Header("Глобальные настройки игры")]
    public List<ElementListLevelSettings> ListLevelSettings;
    CameraMove mCameraMove;

    [Header("Настройки интерфейса")]
    public List<Image> ListImage;

    public GameObject SelectLevel;
    public GameObject GameLevel;
    public GameObject ScorePanel;

    public TextMeshProUGUI LevelName;
    public TextMeshProUGUI RecordLevel;
    public TextMeshProUGUI LoseText;
    public TextMeshProUGUI GameLog;
    public TextMeshProUGUI PlayerID;
    public TextMeshProUGUI TekScore;
    
    public GameObject PanelCongratulations;
    public GameObject PanelLose;
    public GameObject PanelRateGame;


    [Header("Служебные настройки")]
    [SerializeField] int TekLevelID;

    SaveSettings mSaveSettings;
    AnalyticsCustomController mAnalyticsCustomController;
    AudioSource mAudioSource;

    public bool ClassInitiate;


    private void Start()
    {

        InitiateClass();

        try
        {
            StartGame();
        }
        catch (System.Exception e)
        {
            SetLogGame(e.Message);
        }
    }

    public void InitiateClass()
    {
        if (ClassInitiate) return;

        mSaveSettings = GetComponent<SaveSettings>();
        mSaveSettings.InitiateClass();
        //mADSController = GetComponent<ADSController>();
        mAnalyticsCustomController = GetComponent<AnalyticsCustomController>();
        mAudioSource = GetComponent<AudioSource>();

        mCameraMove = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraMove>();

        ClassInitiate = true;
    }

    void StartGame()
    {
        //PlayerID.text = "ID: " + AddLeadingZerosPlayerid(mSaveSettings.GetPlayerID());
        //TekScore.text = "Score: " + mSaveSettings.GetCountCoins().ToString();

        //SelectLevel.SetActive(true);
        //GameLevel.SetActive(false);

        
        InitiateLevelButton();

    }

    void InitiateLevelButton()
    {
        
    }

    public void GreateGameLevel(int LevelNumber = 0)
    {
        
    }

    ElementListLevelSettings GetElementListLevelSettings(int LevelNumber)
    {
        ElementListLevelSettings ReturnValue = null;

        foreach (ElementListLevelSettings TekElementListLevelSettings in ListLevelSettings)
        {
            if (TekElementListLevelSettings.LevelFrom <= LevelNumber && TekElementListLevelSettings.LevelTo >= LevelNumber)
            {
                ReturnValue = TekElementListLevelSettings;
                break;
            }
        }

        return ReturnValue;
    }

    public Vector3 GetLowerLeftCorner()
    {
        return mCameraMove.LowerLeftCorner;
    }

    public Vector3 GetRightUpperCorner()
    {
        return mCameraMove.RightUpperCorner;
    }

    void SetLevelName()
    {
        LevelName.text   = "Level " + TekLevelID.ToString();
        SetCountCoints();
    }

    public void SetCountCoints()
    {
        RecordLevel.text = mSaveSettings.GetCountCoins().ToString();
    }

    void DisactivateImageAndText()
    {
        PanelCongratulations.SetActive(false);
        PanelLose.SetActive(false);        

    }

    void ActivateImageAndText(int LevelNumber)
    {
        if (LevelNumber == 0)
            LevelNumber = 1;

        

    }

    



    //Кликнули на кнопку следующий уровень
    public void StartNextLevel()
    {
        PlayButtonClick();
        //if (mADSController != null) mADSController.ShowADS();
        mSaveSettings.SetMaxOpenLevel(TekLevelID + 1);
        GreateGameLevel(TekLevelID + 1);
    }

    //Кликнули на кнопку перезапуск текущего уровня
    public void RestartTekLevel()
    {
        PlayButtonClick();
        //if (mADSController != null) mADSController.ShowADS();
        GreateGameLevel(TekLevelID);
    }

    public void RestartlastLevel()
    {
        PlayButtonClick();
        //if (mADSController != null) mADSController.ShowADS();
        //int NewLevelid = TekLevelID - 4;
        int NewLevelid = TekLevelID;

        //if (NewLevelid < 1) NewLevelid = 1;

        GreateGameLevel(NewLevelid);
    }

    

    void ShowPanelCongratulations()
    {
        PanelCongratulations.SetActive(true);
        mAnalyticsCustomController.AddAnalyticEventLevelPass(TekLevelID);
        StartCoinsAnimation();
    }

    void ShowPanelLose()
    {
        PanelLose.SetActive(true);
        //GeneralCoins.RemoveCoins(5 * -1);        
    }

    public void SetLogGame(string LogGameText)
    {
        GameLog.text = LogGameText;
    }

    void StartCoinsAnimation()
    {

        
    }

    IEnumerator StartAnimationCoins()
    {
        yield return new WaitForSeconds(0.5f);
        //TekCoins.StartAnimation(TekAwards);
    }

    string AddLeadingZerosPlayerid(int PlayerID)
    {
        string ReturnValue = "0000001";

        int LengtTekPlayeId    = PlayerID.ToString().Length;
        int LengtVerifyPlayeId = ReturnValue.Length;

        string AddZero = "";

        for (int i = 0; i < (LengtVerifyPlayeId - LengtTekPlayeId); i++)
        {
            AddZero = AddZero + "0";
        }

        return AddZero + PlayerID.ToString();
    }

    public void ClickButtonShowScore()
    {

        /*PlayButtonClick();
        if (ScorePanel.activeInHierarchy)
        {
            ScorePanel.SetActive(false);
            return;
        }         

        ScorePanel.SetActive(true);

        List<ElementListSavedScore> NewListSavedSCore = new List<ElementListSavedScore>();

        foreach (ElementListSavedScore SaveElement in mSaveSettings.GetListSavedScore())
        {
            NewListSavedSCore.Add(SaveElement);
        }
            
        ElementListSavedScore PlayerElement = new ElementListSavedScore();
        PlayerElement.id    = mSaveSettings.GetPlayerID();
        PlayerElement.Score = mSaveSettings.GetCountCoins();

        NewListSavedSCore.Add(PlayerElement);

        NewListSavedSCore = NewListSavedSCore.OrderByDescending(t => t.Score).ToList();
        //Дальше надо сформировать список

        bool ThisEventScore = false;
        bool ThisPlayer = false;
        int PlayerID = mSaveSettings.GetPlayerID();

        for (int i = 0; i < NewListSavedSCore.Count; i++)
        {
            ScoreRowController TekRowController = ListScoreRowController[i];
            ElementListSavedScore TekElementListSavedScore = NewListSavedSCore[i];

            ThisPlayer = (TekElementListSavedScore.id == PlayerID);


            TekRowController.SetParametr(i + 1, AddLeadingZerosPlayerid(TekElementListSavedScore.id), TekElementListSavedScore.Score, ThisPlayer, ThisEventScore);

            ThisEventScore = !ThisEventScore;
            
        }*/

    }

    public void PlayButtonClick()
    {
        mAudioSource.Play();
    }

    public void ClickButtonRateGame()
    {
        mSaveSettings.SetClickButtonRateGame();
        PanelRateGame.SetActive(false);
        Application.OpenURL("market://details?id=com.AKGAME.CountCorrectly");

    }

    void OpenPanelRateGame()
    {
        if (mSaveSettings.GetClickButtonRateGame()) return;

        if (TekLevelID == 5 || TekLevelID == 10 || TekLevelID == 15)
        {
            PanelRateGame.SetActive(true);
        }
    }

    public GameObject GetPrefabCharacter(GamerTypeEnum GamerType)
    {

        GameObject ReturnValue = PrefabGamer;

        if (GamerType == GamerTypeEnum.Enemy)
        {
            ReturnValue = PrefabEnemy;
        }
        else
        {
            ReturnValue = PrefabAlly;
        }

        return ReturnValue;

    }

    public GameObject GetParentNewObject(GamerTypeEnum GamerType)
    {

        GameObject ReturnValue = EnemyAndResourcesParent;
        
        return ReturnValue;

    }

}

[System.Serializable]
public class ElementListSavedScore
{
    public int id;
    public int Score;
}

[System.Serializable]
public class ElementListLevelSettings
{
    public int LevelFrom; //Уровень от
    public int LevelTo;   //Уровень До
    public int NumberOfFruit;       //Количество фруктов
    public int NumberOfEmptyBeaker; //Количество пустых мензурок    
}

[System.Serializable]
public enum GamerTypeEnum // Знаки
{
    Gamer = 5,        //Яблоко
    Enemy = 10,       //Лимон
    Ally = 15        //Лайм
    
}

