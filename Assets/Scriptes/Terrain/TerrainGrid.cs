﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGrid : MonoBehaviour
{

    public int CountXDirectionCube;
    public int CountZDirectionCube;
    public GameObject CubePrefab;

    // Start is called before the first frame update
    void Start()
    {
        //GridTerrain();
    }

    void GridTerrain()
    {

        for (int XPosition = 0; XPosition < CountXDirectionCube; XPosition++)
        {
            for (int ZPosition = 0; ZPosition < CountZDirectionCube; ZPosition++)
            {
                GameObject NewObject = Instantiate(CubePrefab, gameObject.transform);

                Vector3 NewPosition = new Vector3(XPosition,0, ZPosition);

                NewObject.transform.SetPositionAndRotation(NewPosition,Quaternion.identity);
                NewObject.gameObject.name = "Cube(" + NewPosition.x.ToString() + ", " + NewPosition.z.ToString() + ")";

            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
