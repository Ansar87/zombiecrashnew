﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

public class CameraMove : MonoBehaviour
{

    [Header("Параметры скорости движения камеры")]
    public float keyMoveSpeed = 15.0f;
    public float mouseMoveSpeed = 5.0f;
    public float ZoomSpeed = 30.0f;

    [Header("Минимальна и максимальная высота камеры")]
    public float MinHeithForTerrain;
    public float MaxHeithForTerrain;
    Vector3 StartPosition;



    [Header("Ссылка на общие объекты")]
    StaticValuesForGame mStaticValuesForGame;

    [SerializeField] Transform MainCameraPlatformHrizontalVerticalmove;
    [SerializeField] Transform TargetTransform;
    [SerializeField] Vector3 OffsetHeroesPositionCamera;

    /*float LastPosotionX;
    float LastPosotionZ;
    [SerializeField] float LastDistanceTwoTouch;
    Transform MoveObject;
    Vector3 ZoomVector;

    float Screenheight;
    float ScreenheightCorrect;
    float Screenwidth;
    float ScreenwidthCorrect;

    float HorizontalPosition;
    float VerticalPosition;*/

    /*float DistanceMove;

    bool ThisHorizontalMove;
    bool ThisVerticalMove;
    bool ThisScroll;*/

    //SettingsCameraPositionGameMode SettingsCameraPosition = null;
    public Vector3 LowerLeftCorner;           //Крайняя Левая точка
    public Vector3 RightUpperCorner;          //Крайняя Правая точка

    //public Vector3 OffsetHeroesPositionCamera;

    bool ClickOccured; //Клик произошел.


    void Start()
    {
        /*MoveObject = transform;
        LastPosotionX = 0f;
        LastDistanceTwoTouch = 0f;
        //ZoomVector = transform.position - GetPositionTerrainCenterMonitor();
        ZoomVector = new Vector3(0f, ZoomVector.y, ZoomVector.z);
        StartPosition = transform.position;
        DistanceMove = 0;

        //mStaticValuesForGame = GameObject.FindGameObjectWithTag("GeneralScript").GetComponent<StaticValuesForGame>();
        //mStaticValuesForGame.InitiateClass();*/


        /*SettingsCameraPosition = mStaticValuesForGame.mUIController.GetSettingsCameraPosition(mStaticValuesForGame.mSaveSettings.GetSelectGameMode());

        if (SettingsCameraPosition != null)
        {

            if (SettingsCameraPosition.LowerLeftCorner == null) SettingsCameraPosition = null;
            else
            {
                //OffsetPositionCamera = new Vector3(SettingsCameraPosition.LowerLeftCorner.position.x * -1, 0, SettingsCameraPosition.LowerLeftCorner.position.z * -1);
                /*LowerLeftCorner  = SettingsCameraPosition.LowerLeftCorner.position + OffsetPositionCamera;
                RightUpperCorner = SettingsCameraPosition.RightUpperCorner.position + OffsetPositionCamera;
                LowerLeftCorner  = SettingsCameraPosition.LowerLeftCornerPosition;
                RightUpperCorner = SettingsCameraPosition.RightUpperCornerPosition;
                OffsetHeroesPositionCamera = SettingsCameraPosition.HeroesOffset;

            //}

        }*/

        /*Screenwidth = Screen.width;
        ScreenwidthCorrect = Screenwidth * 0.07f;
        Screenheight = Screen.height;
        ScreenheightCorrect = Screenheight * 0.15f;*/

        SetCameraForHeroes();

    }

    void Update()
    {

        //HeroesCameraIsometricMove();

        /*if (mStaticValuesForGame.IsometricModeEnabled)
        {
            if (mStaticValuesForGame.TekCharacterProperties != null && mStaticValuesForGame.TekCharacterProperties.mHeroesIconController)
            {
                HeroesCameraIsometricMove();
            }
        }        
        else if (Input.touchCount == 2 || Input.GetAxis("Mouse ScrollWheel") != 0)
        {

            /*ThisScroll = true;

            Vector3 NewPosition = Vector3.zero;

            if (Input.GetAxis("Mouse ScrollWheel") != 0)
            {

                if (Input.GetAxis("Mouse ScrollWheel") > 0)
                {
                    NewPosition = new Vector3(0, 0, 2);
                }
                else
                {
                    NewPosition = new Vector3(0, 0, -2);
                }

                float DistanceY = StartPosition.y - transform.position.y;

                if (Mathf.Abs(DistanceY) < 50 //1. Дистанция меньше 30, всё хорошо.
                    || (DistanceY > 0 && NewPosition.z < 0) //2. Дистанция больше 30 и мы идем вниз
                    || (DistanceY < 0 && NewPosition.z > 0)) //3. Дистанция меньше 0 и мы идем вверх
                {
                    if (SettingsCameraPosition != null && CanMoveTheCamera(NewPosition,MoveObject.position) == false) return;
                    MoveObject.Translate(NewPosition);
                    UpdateHealthBarBuildingPosition();
                }

                return;

            }
            else
            {

                Touch touch0 = Input.GetTouch(0);
                Touch touch1 = Input.GetTouch(1);

                if (LastDistanceTwoTouch == 0f)
                {
                    LastDistanceTwoTouch = Mathf.Abs(Vector2.Distance(touch0.position, touch1.position));
                }
                else
                {
                    float NewDistance = Mathf.Abs(Vector2.Distance(touch0.position, touch1.position));

                    if (LastDistanceTwoTouch < NewDistance)
                    {
                        NewPosition = new Vector3(0, 0, 2);
                    }
                    else if (LastDistanceTwoTouch > NewDistance)
                    {
                        NewPosition = new Vector3(0, 0, -2);
                    }

                    float DistanceY = StartPosition.y - transform.position.y;

                    if (Mathf.Abs(DistanceY) < 30 //1. Дистанция меньше 30, всё хорошо.
                    || (DistanceY > 0 && NewPosition.z < 0) //2. Дистанция больше 30 и мы идем вниз
                    || (DistanceY < 0 && NewPosition.z > 0)) //3. Дистанция меньше 0 и мы идем вверх
                    {
                        if (SettingsCameraPosition != null && CanMoveTheCamera(NewPosition,MoveObject.position) == false) return;
                        MoveObject.Translate(NewPosition);
                    }
                    LastDistanceTwoTouch = NewDistance;

                }

            }

            UpdateHealthBarBuildingPosition();

            return;

        }

        else if (Input.touchCount == 1 || Input.GetAxis("MouseLeftClick") != 0 || Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
        {
            HorizontalMove();
        }
        else if (Input.touchCount == 0 && Input.GetAxis("MouseLeftClick") == 0)
        {
            LastDistanceTwoTouch = 0f;
            ClickOccured = false;
            ThisHorizontalMove = false;
            ThisVerticalMove = false;
            ThisScroll = false;
            LastPosotionX = 0;
            LastPosotionZ = 0;
        }*/

        HeroesCameraIsometricMove();

    }

    void HorizontalMove()
    {

        /*if (ThisScroll) { return; }

        if ((Input.touchCount == 1 || Input.GetAxis("MouseLeftClick") != 0)
        && ClickOccured == false
        && ThisHorizontalMove == false
        && ThisVerticalMove == false)
        {

            if (Input.touchCount > 0)
            {
                if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)) { return; }
            }

            if (IsPointerOverUIObject()) return;

            RaycastHit Hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            ClickOccured = true;
            if (Physics.Raycast(ray, out Hit))
            {
                Vector3 targetPoint = Hit.point;
                Transform transformPoint = Hit.collider.transform;

                BuildSpawnController mBuildSpawnController = transformPoint.GetComponent<BuildSpawnController>();
                BuildProperties mBuildProperties = transformPoint.GetComponent<BuildProperties>();
                CharacterPropertiesController mCharacterPropertiesController = transformPoint.GetComponent<CharacterPropertiesController>();

                if (mBuildSpawnController != null)
                {
                    mBuildSpawnController.ToSelectABuildingForBuilding();
                    return;
                }
                else if (mBuildProperties != null)
                {
                    if (mStaticValuesForGame.SelectGameMode == SelectGameModeEnum.CentralBase)
                    {
                        if (mBuildProperties.UpgradeTypeObject == UpgradeTypeObjectEnum.PeopleTownHall)
                        {
                            PanelLevelUpgradeController mUnitLevelUpgradeController = mStaticValuesForGame.mPanelLevelUpgradeController;
                            mUnitLevelUpgradeController.ThisLevelUpgrade = false;
                            mUnitLevelUpgradeController.OpenPanelActionToUpgradeLevel(null, false, true, false);
                            mStaticValuesForGame.mUIController.SetTekActivePanel(mUnitLevelUpgradeController.gameObject);
                            mStaticValuesForGame.mSoundController.ToPlayMusicUI(mStaticValuesForGame.mSoundController.MenuButtonClick);
                        }
                        else if (mBuildProperties.UpgradeTypeObject == UpgradeTypeObjectEnum.PeopleBarracks)
                        {
                            PanelLevelUpgradeController mUnitLevelUpgradeController = mStaticValuesForGame.mPanelLevelUpgradeController;
                            mUnitLevelUpgradeController.InitiateClass();
                            mUnitLevelUpgradeController.ThisLevelUpgrade = false;
                            mUnitLevelUpgradeController.TekActiveUpgradeTypeObject = UpgradeTypeObjectEnum.PeopleWorker;
                            mUnitLevelUpgradeController.TekActiveCharacterType = CharacterTypeEnum.Worker;
                            //mUnitLevelUpgradeController.InitiateCharacterProperities(mUnitLevelUpgradeController.GetGeneralCharacterProperties(), true);
                            mUnitLevelUpgradeController.OpenPanelActionToUpgradeLevel(mUnitLevelUpgradeController.GetGeneralCharacterProperties(), true, false, true);
                            mStaticValuesForGame.mUIController.SetTekActivePanel(mUnitLevelUpgradeController.gameObject);
                            mStaticValuesForGame.mSoundController.ToPlayMusicUI(mStaticValuesForGame.mSoundController.MenuButtonClick);
                        }
                        else if (mBuildProperties.UpgradeTypeObject == UpgradeTypeObjectEnum.PeopleBlacksmith)
                        {
                            mStaticValuesForGame.mUIController.OpenPanelBlacksmith();
                        }
                    }
                    else
                    {
                        mBuildProperties.SelectThisBuildAsEnemy();
                    }
                    return;
                }
                else if (mCharacterPropertiesController != null)
                {
                    if (mStaticValuesForGame.SelectGameMode == SelectGameModeEnum.CentralBase)
                    {
                        PanelLevelUpgradeController mUnitLevelUpgradeController = mStaticValuesForGame.mPanelLevelUpgradeController;
                        mUnitLevelUpgradeController.ThisLevelUpgrade = true;
                        mUnitLevelUpgradeController.OpenPanelActionToUpgradeLevel(mCharacterPropertiesController);
                        mStaticValuesForGame.mUIController.SetTekActivePanel(mUnitLevelUpgradeController.gameObject);
                        mStaticValuesForGame.mSoundController.ToPlayMusicUI(mStaticValuesForGame.mSoundController.MenuButtonClick);
                    }
                    else
                    {
                        mCharacterPropertiesController.SelectThisAsEnemy();
                    }

                    return;
                }
                else
                {
                    CentralBaseInteractiveObject TekCentralBaseInteractiveObject = transformPoint.GetComponent<CentralBaseInteractiveObject>();

                    if (TekCentralBaseInteractiveObject != null && TekCentralBaseInteractiveObject.InteractiveObjectType == InteractiveObjectTypeEnum.PortalHistory)
                    {
                        mStaticValuesForGame.mUIController.LoadSceneSelecteLevelCampaight();
                    }
                    else if (TekCentralBaseInteractiveObject != null && TekCentralBaseInteractiveObject.InteractiveObjectType == InteractiveObjectTypeEnum.BilboardArcade)
                    {
                        mStaticValuesForGame.mSoundController.ToPlayMusicUI(mStaticValuesForGame.mSoundController.MenuButtonClick);
                        mStaticValuesForGame.mUIController.OpenPanelSelectArcadeTasks();
                    }
                    else if (TekCentralBaseInteractiveObject != null && TekCentralBaseInteractiveObject.InteractiveObjectType == InteractiveObjectTypeEnum.BilboardPVP)
                    {
                        mStaticValuesForGame.mUIController.OpenPanelSelectPVPBattle();
                    }
                    else if (TekCentralBaseInteractiveObject != null && TekCentralBaseInteractiveObject.InteractiveObjectType == InteractiveObjectTypeEnum.WeaponAndArmorShop)
                    {
                        mStaticValuesForGame.mUIController.OpenPanelShopGameValute();
                    }
                    else if (TekCentralBaseInteractiveObject != null && TekCentralBaseInteractiveObject.InteractiveObjectType == InteractiveObjectTypeEnum.GemsAndGoldShop)
                    {
                        mStaticValuesForGame.mUIController.OpenPanelRealGameValute();
                    }

                }


                if (transformPoint.GetComponent<LootBag>() != null)
                {
                    transformPoint.GetComponent<LootBag>().TransferAllEquipmentToInventory();
                    return;
                }

            }

        }

        VerticalPosition = 0f;
        HorizontalPosition = 0f;

        //Новый код от 22.11.2019
        HorizontalPosition = GetHorizontalPosition() / 10;
        VerticalPosition = GetVerticalPosition() / 15;

        Vector3 TranslateVector = new Vector3(HorizontalPosition, 0, VerticalPosition*-1) * mouseMoveSpeed;

        if (SettingsCameraPosition != null && CanMoveTheCamera(TranslateVector,MainCameraPlatformHrizontalVerticalmove.position) == false) return;

        if (TranslateVector != Vector3.zero)
        {
            MainCameraPlatformHrizontalVerticalmove.Translate(TranslateVector);
            UpdateHealthBarBuildingPosition();
        }

        //Старый код
        /*if (ThisVerticalMove == false)
        {
            HorizontalPosition = GetHorizontalPosition();
        }

        if (ThisHorizontalMove == false)
        {
            VerticalPosition = GetVerticalPosition();
        }

        if (ThisHorizontalMove == false && ThisVerticalMove == false)
        {

            if ((Mathf.Abs(HorizontalPosition) * 3f) > Mathf.Abs(VerticalPosition))
            {

                ThisHorizontalMove = true;
                VerticalPosition = 0;
            }
            else if ((Mathf.Abs(HorizontalPosition) * 3f) < Mathf.Abs(VerticalPosition))
            {
                ThisVerticalMove = true;
                HorizontalPosition = 0;
            }

        }

        if (ThisHorizontalMove)
        {
            Vector3 TranslateVector = new Vector3(HorizontalPosition, 0, 0) * mouseMoveSpeed;

            if (SettingsCameraPosition != null && CanMoveTheCamera(TranslateVector) == false) return;

            if (TranslateVector != Vector3.zero)
            {
                MoveObject.Translate(TranslateVector);
                UpdateHealthBarBuildingPosition();
            }

        }

        if (ThisVerticalMove)
        {
            if (VerticalPosition != 0)
            {
                transform.rotation = transform.rotation * Quaternion.Euler(VerticalPosition, 0f, 0f);
                UpdateHealthBarBuildingPosition();
            }
        }*/


    }

    /*bool CanMoveTheCamera(Vector3 TranslateVector, Vector3 PositionMoveObject)
    {

        bool ReturnValue = true;

        //Vector3 NewCameraPosition = PositionMoveObject + TranslateVector + OffsetPositionCamera;
        Vector3 NewCameraPosition = PositionMoveObject + TranslateVector;
        if (NewCameraPosition.x >= LowerLeftCorner.x && NewCameraPosition.z >= LowerLeftCorner.z
        && NewCameraPosition.x <= RightUpperCorner.x && NewCameraPosition.z <= RightUpperCorner.z)
        {
            ReturnValue = true;
        }
        else
        {
            ReturnValue = false;
        }

        return ReturnValue;

    }

    private bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    public void SelectEnemy(GameObject GameObjectEnemy, GamerTypeEnum GamerType)
    {
        if (GamerType == GamerTypeEnum.Gamer)
        {
            return;
        }



    }

    public void UpdateHealthBarBuildingPosition()
    {



    }

    float GetHorizontalPosition()
    {

        float NewPosition = 0;

        if (Input.GetMouseButtonDown(0))
        {
            LastPosotionX = Input.mousePosition.x;
            ClickOccured = true;
            return NewPosition;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            LastPosotionX = 0f;
            LastDistanceTwoTouch = 0f;
            ClickOccured = false;
            return NewPosition;
        }
        else if (LastPosotionX != 0f)
        {   

            NewPosition = LastPosotionX - Input.mousePosition.x;

            if(NewPosition < 0 ) NewPosition = NewPosition * -1;
            
            if(NewPosition < ScreenwidthCorrect) 
            {
                NewPosition = 0;
            }
            else
            {
                NewPosition = LastPosotionX - Input.mousePosition.x;
            }
            
            NewPosition = NewPosition * Time.deltaTime;

            if(mStaticValuesForGame.mSaveSettings.InvertCameraMove) NewPosition = NewPosition * -1;
        }
        else
        {
            NewPosition = Input.GetAxis("Horizontal");
        }

        return NewPosition;
    }

    float GetVerticalPosition()
    {

        float NewPosition = 0;

        if (Input.GetMouseButtonDown(0))
        {
            LastPosotionZ = Input.mousePosition.y;
            return NewPosition;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            LastPosotionZ = 0f;
            LastDistanceTwoTouch = 0f;
            return NewPosition;
        }
        else if (LastPosotionZ != 0f)
        {

            NewPosition = Input.mousePosition.y - LastPosotionZ;

            if(NewPosition < 0) NewPosition = NewPosition * -1;
            if(NewPosition < ScreenheightCorrect) NewPosition = 0;

            if(Input.mousePosition.y - LastPosotionZ < 0) NewPosition = NewPosition * -1;    

            NewPosition = NewPosition * Time.deltaTime;

            if(mStaticValuesForGame.mSaveSettings.InvertCameraMove) NewPosition = NewPosition * -1; 

        }
        else
        {
            NewPosition = Input.GetAxis("Vertical");
        }

        return NewPosition;
    }

    public Vector3 GetPositionTerrainCenterMonitor()
    {
        Vector3 ReturnValue = Vector3.zero;

        //1. Определим центр экрана.
        RaycastHit[] _HitArray;
        Ray _ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
        _HitArray = Physics.RaycastAll(_ray, 1000);

        if (_HitArray.Length == 0) { return ReturnValue; }

        RaycastHit HitTerrain = new RaycastHit();

        foreach (var TekRaycast in _HitArray)
        {
            if (TekRaycast.collider.name == "Terrain")
            {
                HitTerrain = TekRaycast;
                break;
            }
        }

        if (HitTerrain.point == null) { return ReturnValue; }

        ReturnValue = HitTerrain.point;

        return ReturnValue;

    }*/

    public void HeroesCameraIsometricMove()
    {
        //Vector3 NewPosition = new Vector3();
        Vector3 NewPosition = (TargetTransform.position + OffsetHeroesPositionCamera) - MainCameraPlatformHrizontalVerticalmove.position;

        if (GetXValidatePosition(NewPosition.x + MainCameraPlatformHrizontalVerticalmove.position.x) == false) NewPosition.x = 0;
        if (GetZValidatePosition(NewPosition.z + MainCameraPlatformHrizontalVerticalmove.position.z) == false) NewPosition.z = 0;

        if (NewPosition.z != 0 || NewPosition.x != 0)
        {
            MainCameraPlatformHrizontalVerticalmove.transform.Translate(NewPosition.x, 0, NewPosition.z);
        }

    }

    public void SetCameraForHeroes()
    {
        //Vector3 NewPosition = (TargetTransform.position + OffsetHeroesPositionCamera) - MainCameraPlatformHrizontalVerticalmove.position;
        //Vector3 NewPosition = new Vector3();
        Vector3 NewPosition = (TargetTransform.position + OffsetHeroesPositionCamera);
        MainCameraPlatformHrizontalVerticalmove.transform.position = NewPosition;  //.  Translate(NewPosition.x, 0, NewPosition.z);


    }

    bool GetXValidatePosition(float XPosition)
    {
        bool ReturnValue = false;

        if (XPosition >= LowerLeftCorner.x && XPosition <= RightUpperCorner.x)
        {
            ReturnValue = true;
        }

        return ReturnValue;
    }

    bool GetZValidatePosition(float ZPosition)
    {
        bool ReturnValue = false;

        if (ZPosition >= LowerLeftCorner.z && ZPosition <= RightUpperCorner.z)
        {
            ReturnValue = true;
        }

        return ReturnValue;
    }

}
