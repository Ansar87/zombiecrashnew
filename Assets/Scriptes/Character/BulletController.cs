﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{

    public CharacterProperties mCharacterProperties;
    public int Damage;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void BulletStart()
    {
        StartCoroutine(SelectedTarget());
    }

    IEnumerator SelectedTarget()
    {
        yield return new WaitForSeconds(5f);
        Destroy(gameObject);        
    }

    void OnTriggerEnter(Collider EnemyColider)
    {

        CharacterProperties TargetCharacterProperties = EnemyColider.gameObject.GetComponent<CharacterProperties>();

        if (TargetCharacterProperties != null)
        {

            if (TargetCharacterProperties.IsDead) return;

            TargetCharacterProperties.SetHit(Damage, mCharacterProperties);
        }

        //Destroy(gameObject);

    }

}
