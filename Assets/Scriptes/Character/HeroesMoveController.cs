﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityStandardAssets.CrossPlatformInput;

public class HeroesMoveController : MonoBehaviour
{

    [SerializeField] OtherUnitsMove ParentOtherUnitsMove; // Здесь хранится список целей которые рядом.
    StaticValuesForGame mStaticValuesForGame;
    SoundController mSoundController;
    CharacterController mCharacterController;
    CharacterProperties mCharacterProperties;
    Vector3 Direction;
    Vector3 LastDirection;
    AudioSource mAudioSource;
    bool FootStepPlay; //Звук ходьбы проигрывается.

    public float Speed;
    float PositionX;
    float PositionY;
    bool MustStandStill;
    Animator mAnimator;
    string SetTriger;
    public bool ThisActive; //Говорит о том что данный герой активен.
    string LastSetBoolParametr;

    float TimeFootStep;

    Vector2 FromVector = Vector2.up;
    //Vector2 FromVector = new Vector2();

    float TimeLastLogDebug = 0;

    bool MoveAndTurnSeparately = false;

    Vector3 DirectionGravitation = new Vector3(0, -9.8f, 0);

    float MaxSpeedCharacter = 6f;

    public CharacterProperties TekSelectedTargetCP;
    public Transform TekSelectedTargetTransform;

    //ВРЕМЯ АТАКИ
    float TimeForLastAtack = 0f;
    float TimeBetweenAtacks = 0f;
    int Damage= 0;

    float AttackRange = 0f;

    // Use this for initialization
    void Start()
    {
        mCharacterController = GetComponent<CharacterController>();
        mCharacterProperties = GetComponent<CharacterProperties>();
        ParentOtherUnitsMove = GetComponentInParent<OtherUnitsMove>();

        mStaticValuesForGame = GameObject.FindGameObjectWithTag("GeneralScript").GetComponent<StaticValuesForGame>();
        mAudioSource = GetComponent<AudioSource>();
        mAnimator = GetComponent<Animator>();
        Speed = 6f;

        ThisActive = (mCharacterProperties.GamerType == GamerTypeEnum.Gamer);

        if (ThisActive)
        {
            TekSelectedTargetCP = null;
            TekSelectedTargetTransform = null;
            StartCoroutine(SelectedTarget());
            UpdateBattleProperties();
        }

    }

    // Update is called once per frame
    void Update()
    {

        if (ThisActive == false) { return; }
        Direction = DirectionGravitation;
        TimeForLastAtack += Time.deltaTime;

        float HeroesHorizontal = 0;
        float HeroesVertical = 0;

        float HeroesIsMove = 0;

        HeroesHorizontal = CrossPlatformInputManager.GetAxis("HeroesHorizontal");
        HeroesVertical = CrossPlatformInputManager.GetAxis("HeroesVertical");

        float AbsHeroesVertical = Mathf.Abs(HeroesVertical);
        float AbsHeroesHorizontal = Mathf.Abs(HeroesHorizontal);

        HeroesIsMove = Mathf.Abs(AbsHeroesVertical) + Mathf.Abs(AbsHeroesHorizontal);

        if (HeroesIsMove > 0)
        {
            MustStandStill = false;
            Vector2 ToVector = FromVector;

            if (HeroesHorizontal != 0 || HeroesVertical != 0)
            {
                ToVector = new Vector2(HeroesHorizontal * 10, HeroesVertical * 10) + FromVector;
                MoveAndTurnSeparately = false;
            }

            float AngleVector = (int)Vector2.SignedAngle(FromVector, ToVector) * -1;
            if (AngleVector < 0) AngleVector = 360 + AngleVector; // Тут будет число от 181 до 359

            float TekAngleTransform = transform.rotation.eulerAngles.y;
            if (TekAngleTransform < 0) TekAngleTransform = 360 + TekAngleTransform;

            float Angle = AngleVector - TekAngleTransform;
            float absAngle = Mathf.Abs(Angle);

            if (absAngle > 10)
            {
                int CorrectAngleDirection = 1;

                if (absAngle > 180) CorrectAngleDirection *= -1;
                if (Angle < 0) CorrectAngleDirection *= -1;

                transform.Rotate(0, 540 * Time.deltaTime * CorrectAngleDirection, 0);
            }

            TimeLastLogDebug += Time.deltaTime;

            if (MoveAndTurnSeparately == false)
            {
                float XSpeed = Mathf.Max(Mathf.Abs(HeroesHorizontal), Mathf.Abs(HeroesVertical));
                Direction = transform.TransformDirection(new Vector3(0, -9.8f, XSpeed)) * Time.deltaTime * Speed * Time.timeScale;
            }
            else
            {
                float AngleRadian = AngleVector * Mathf.Deg2Rad;
                float NewHorizontal = HeroesHorizontal * Mathf.Cos(AngleRadian) - HeroesVertical * Mathf.Sin(AngleRadian);
                float NewVertical = HeroesHorizontal * Mathf.Sin(AngleRadian) + HeroesVertical * Mathf.Cos(AngleRadian);

                Direction = new Vector3((NewHorizontal), -9.8f, (NewVertical)) * Time.deltaTime;
                Direction = transform.TransformDirection(Direction) * Speed * Time.timeScale;
            }

            mCharacterController.Move(Direction);
        }
        else
        {
            /*float InsertAttackRange = 0;

            remainingDistance = mNavMeshAgent.remainingDistance;
            //AttackRange = mNavMeshAgent.stoppingDistance;

            if (mNavMeshAgent.remainingDistance != 0 && mNavMeshAgent.remainingDistance <= mNavMeshAgent.stoppingDistance + InsertAttackRange)
            {
                TimeForLastAtack >= TimeBetweenAtacks*/
            if (TekSelectedTargetTransform != null && TimeForLastAtack >= TimeBetweenAtacks)
            {
                if (Vector3.Distance(TekSelectedTargetTransform.position, transform.position) <= AttackRange)
                {
                    StartAnimationForAtack(TekSelectedTargetTransform);
                    TimeForLastAtack = 0;
                }
            }

        }

        SetSpeedCharacter(AbsHeroesHorizontal, AbsHeroesVertical);
    }

    public void StartAnimationForAtack(Transform TargetAttack)
    {

        Vector3 NewTargetPosition = new Vector3(TekSelectedTargetTransform.position.x, transform.position.y, TekSelectedTargetTransform.position.z);
        transform.LookAt(NewTargetPosition);

        //Debug.Log(TargetAttack.position);
        mAnimator.SetInteger("ActionNumber", Random.Range(0, GetCountAttackAnimation()));
        mAnimator.SetTrigger("BaseAttack");
        StartSoundEffect();

    }

    public void UpdateBattleProperties()
    {
        AttackRange = GetAttakRange();
        TimeBetweenAtacks = GetTimeBetweenAtacks();
        Damage = GetDamage();
    }

    float GetAttakRange()
    {
        return mCharacterProperties.GetAttcakRange();
    }

    int GetCountAttackAnimation()
    {
        return mCharacterProperties.GetCountAttackAnimation();
    }

    float GetTimeBetweenAtacks()
    {
        return mCharacterProperties.GetTimeBetweenAtacks();
    }

    int GetDamage()
    {
        return mCharacterProperties.GetDamage();
    }

    void SetSpeedCharacter(float AbsHeroesHorizontal, float AbsHeroesVertical)
    {
        float MaxValue = Mathf.Max(AbsHeroesHorizontal, AbsHeroesVertical);

        mAnimator.SetFloat("Speed", MaxSpeedCharacter * MaxValue);

    }

    public void FootL()
    {

    }

    public void FootR()
    {

    }

    public void Hit()
    {   
        if (TekSelectedTargetCP != null)
        {

            if (TekSelectedTargetCP.IsDead)
            {
                ParentOtherUnitsMove.DeleteInTargetList(TekSelectedTargetCP);
            }
            else
            {
                TekSelectedTargetCP.SetHit(mCharacterProperties.GetDamage(),mCharacterProperties);
            }

        }
    }

    public void Shoot()
    {
        //yield return new WaitForSeconds(0.3f);

        if (TekSelectedTargetTransform == null) return;

        GameObject Bullet = Instantiate(mCharacterProperties.BulletPrefab, mCharacterProperties.BulletPrefabPosition.position, Quaternion.identity) as GameObject;
        BulletController mBulletController = Bullet.GetComponent<BulletController>();
        mBulletController.Damage = Damage;
        mBulletController.mCharacterProperties = mCharacterProperties;

        //mBulletController.

        Vector3 TargetPosition = TekSelectedTargetTransform.position + Vector3.up;

        //Vector3 force = ((TargetPosition - Bullet.transform.position) / Vector3.Distance(TargetPosition, Bullet.transform.position)) * 60;
        Vector3 force = ((TargetPosition - Bullet.transform.position) / Vector3.Distance(TargetPosition, Bullet.transform.position)) * 60;
        Bullet.SetActive(true);
        Bullet.GetComponent<Rigidbody>().AddForce(force * 1, ForceMode.Impulse);
        mBulletController.BulletStart();

    }

    public void StartSoundEffect()
    {
        mCharacterProperties.StartSoundEffect();
    }

    IEnumerator SelectedTarget()
    {
        yield return new WaitForSeconds(1f);

        if (ParentOtherUnitsMove.TargetList.Count == 0)
        {
            if (TekSelectedTargetCP != null)
            {
                TekSelectedTargetCP.SelectedTarget(false);
                TekSelectedTargetCP = null;
                TekSelectedTargetTransform = null;
            }
        }
        else
        {

            if (TekSelectedTargetCP != null)
            {
                TekSelectedTargetCP.SelectedTarget(false);
            }

            List<CharacterProperties> SortingList = ParentOtherUnitsMove.TargetList.OrderBy(t => Vector3.Distance(t.transform.position, transform.position)).ToList();
            TekSelectedTargetCP = SortingList[0];
            TekSelectedTargetCP.SelectedTarget(true);
            TekSelectedTargetTransform = TekSelectedTargetCP.transform;
        }

        StartCoroutine(SelectedTarget());
    }

    /*
    public void SetTrigerAndSpeedAnimator(string TriggerName)
    {
        if (SetTriger != TriggerName)
        {
            if (SetTriger != "")
            {
                mAnimator.ResetTrigger(SetTriger);
            }
            mAnimator.SetTrigger(TriggerName);
            SetTriger = TriggerName;
        }

    }

    public void SetBoolAnimator(string ParametrName, bool NewValue)
    {
        mAnimator.SetBool(ParametrName, NewValue);
        if (NewValue == true)
        {
            LastSetBoolParametr = ParametrName;
        }

    }
    
    public void SetAttackAction(float SetValue)
    {
        mAnimator.SetFloat("AttackAction", SetValue);
    }

    public void SetAttackFalshe()
    {
        mAnimator.SetBool(LastSetBoolParametr, false);
        mStaticValuesForGame.TekMoveAndAttack.AttackAllowed = false;
    }

    void SetSpeedCharacter(float HeroesHorizontal, float HeroesVertical)
    {

        if (MustStandStill)
        {
            Speed = 0f;
            PositionX = 0f;
            PositionY = 0f;
        }
        else
        {

            float SpeedKoef = 0f;

            if (Mathf.Abs(HeroesHorizontal) > Mathf.Abs(HeroesVertical))
            {
                SpeedKoef = Mathf.Abs(HeroesHorizontal) / 1.5f;
            }
            else if (HeroesVertical < 0)
            {
                SpeedKoef = Mathf.Abs(HeroesVertical) / 1.5f;
            }
            else
            {
                SpeedKoef = HeroesVertical;
            }

            Speed = mCharacterProperties.MaxSpeedValue - (mCharacterProperties.MaxSpeedValue - mCharacterProperties.MinSpeedValue) * mCharacterProperties.GetLostHealthPercentage();
            Speed = Speed * SpeedKoef;

            PositionX = (HeroesVertical * 6) - (mCharacterProperties.MaxSpeedValue - mCharacterProperties.MinSpeedValue) * mCharacterProperties.GetLostHealthPercentage();
            PositionY = (HeroesHorizontal * 6) - (mCharacterProperties.MaxSpeedValue - mCharacterProperties.MinSpeedValue) * mCharacterProperties.GetLostHealthPercentage();

            if (mStaticValuesForGame.IsometricModeEnabled)
            {
                PositionX = Mathf.Max(Mathf.Abs(PositionX), Mathf.Abs(PositionY));
                PositionY = 0;
            }

        }

        mAnimator.SetFloat("Speed", Speed);
        mAnimator.SetFloat("PositionX", PositionX);
        mAnimator.SetFloat("PositionY", PositionY);
    }
    */
}
