﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterProperties : MonoBehaviour
{

    public GamerTypeEnum GamerType;
    public bool IsDead;
    int TekHealth;
    int Damage;

    [SerializeField] List<WeaponProperties> ListWeaponProperties;

    [Header("Справочные показатели")]
    public int CountAttackAnimationZombie;
    public int CountAttackAnimationAssaultRiffle;
    public int CountAttackAnimationMace;
    public int CountAttackAnimationUnarmed;

    public float AttackRangeZombie;
    public float AttackRangeUnarmed;
    public float AttackRangeAssaultRiffle;
    public float AttackRangeMace;

    public float AttackTimeZombie;
    public float AttackTimeUnarmed;
    public float AttackTimeAssaultRiffle;
    public float AttackTimeMace;

    public int DamageZombie;
    public int DamageUnarmed;
    public int DamageAssaultRiffle;
    public int DamageMace;

    [Header("Материалы")]
    public Material ZombieMaterial;
    public Material GamerMaterial;

    [Header("Система частиц")]
    public GameObject TargetSelected;
    public GameObject FXDeadGameObject;
    public GameObject FXGetHitGameObject;
    public GameObject FXDeadBlood;

    public bool ThisZombie;
    public GameObject BulletPrefab;
    public Transform BulletPrefabPosition;

    Animator mAnimator;
    public OtherUnitsMove mOtherUnitsMove;

    void Start()
    {
        mAnimator       = GetComponent<Animator>();
        mOtherUnitsMove = GetComponent<OtherUnitsMove>();

        AttackRangeZombie = 1f;
        AttackRangeUnarmed = 1f;
        CountAttackAnimationZombie = 1;
        CountAttackAnimationUnarmed = 1;
        AttackTimeZombie = 3f;
        AttackTimeUnarmed = 1.5f;
        SetMoveType();
        SetMaterial();

        if (ThisZombie)
        {
            TekHealth = 10;
            //Damage = 5;
        }
        else
        {
            TekHealth = 100;
            //Damage    = 7;
        }
    }

    void Update()
    {

    }

    public void SetMoveType()
    {
        if (ThisZombie)
        {
            mAnimator.SetBool("Zombie", true);
            return;
        }

        if (GamerType == GamerTypeEnum.Gamer)
        {

            WeaponTypeEnum WeaponType = GetActiveWeaponType();
            if (WeaponType == WeaponTypeEnum.Unarmed)
            {
                mAnimator.SetBool("Unarmed", true);
            }
            else if (WeaponType == WeaponTypeEnum.MaceLucil)
            {
                mAnimator.SetBool("MaceOneHand", true);
            }
            else if (WeaponType == WeaponTypeEnum.AssaultRiifleTypeOne)
            {
                mAnimator.SetBool("AssaultRiifle", true);
            }

        }
        else if (GamerType == GamerTypeEnum.Enemy)
        {

        }

    }

    public void SetMaterial()
    {
        Material SelectedMaterial = null;

        if (ThisZombie)
        {
            SelectedMaterial = ZombieMaterial;
        }
        else
        {
            SelectedMaterial = GamerMaterial;
        }

        GetComponentInChildren<SkinnedMeshRenderer>().material = SelectedMaterial;

    }

    public void SelectedTarget(bool SetValue)
    {
        TargetSelected.SetActive(SetValue);

        if (!ThisZombie)
        {
            Debug.Log("sdfsf");
        }

    }

    public int GetCountAttackAnimation()
    {
        int ReturnValue = 0;

        if (ThisZombie)
        {
            ReturnValue = CountAttackAnimationZombie;
        }
        else
        {
            if (GamerType == GamerTypeEnum.Gamer)
            {

                WeaponTypeEnum WeaponType = GetActiveWeaponType();
                if (WeaponType == WeaponTypeEnum.Unarmed)
                {
                    ReturnValue = CountAttackAnimationUnarmed;
                }
                else if (WeaponType == WeaponTypeEnum.MaceLucil)
                {
                    ReturnValue = CountAttackAnimationMace;
                }
                else if (WeaponType == WeaponTypeEnum.AssaultRiifleTypeOne)
                {
                    ReturnValue = CountAttackAnimationAssaultRiffle;
                }

            }
            
        }

        return ReturnValue;
    }

    public float GetTimeBetweenAtacks()
    {
        float ReturnValue = 0f;

        if (ThisZombie)
        {
            ReturnValue = AttackTimeZombie;
        }
        else
        {
            WeaponTypeEnum WeaponType = GetActiveWeaponType();
            if (WeaponType == WeaponTypeEnum.Unarmed)
            {
                ReturnValue = AttackTimeUnarmed;
            }
            else if (WeaponType == WeaponTypeEnum.MaceLucil)
            {
                ReturnValue = AttackTimeMace;
            }
            else if (WeaponType == WeaponTypeEnum.AssaultRiifleTypeOne)
            {
                ReturnValue = AttackTimeAssaultRiffle;
            }            
        }

        return ReturnValue;
    }

    public float GetAttcakRange()
    {
        float ReturnValue = 0f;

        if (ThisZombie)
        {
            ReturnValue = AttackRangeZombie;
        }
        else
        {
            WeaponTypeEnum WeaponType = GetActiveWeaponType();
            if (WeaponType == WeaponTypeEnum.Unarmed)
            {
                ReturnValue = AttackRangeUnarmed;
            }
            else if (WeaponType == WeaponTypeEnum.MaceLucil)
            {
                ReturnValue = AttackRangeMace;
            }
            else if (WeaponType == WeaponTypeEnum.AssaultRiifleTypeOne)
            {
                ReturnValue = AttackRangeAssaultRiffle;
            }            
        }

        return ReturnValue;
    }

    public int GetDamage()
    {
        int ReturnValue = 0;

        if (ThisZombie)
        {
            ReturnValue = DamageZombie;
        }
        else
        {
            WeaponTypeEnum WeaponType = GetActiveWeaponType();
            if (WeaponType == WeaponTypeEnum.Unarmed)
            {
                ReturnValue = DamageUnarmed;
            }
            else if (WeaponType == WeaponTypeEnum.MaceLucil)
            {
                ReturnValue = DamageMace;
            }
            else if (WeaponType == WeaponTypeEnum.AssaultRiifleTypeOne)
            {
                ReturnValue = DamageAssaultRiffle;
            }
        }

        return ReturnValue;
    }

    public void StartSoundEffect()
    {

    }

    public void SetHit(int HitDamage, CharacterProperties AttackedCharacterProperties)
    {
        TekHealth -= HitDamage;
        if (TekHealth <= 0)
        {
            IsDead = true;
            AttackedCharacterProperties.mOtherUnitsMove.DeleteInTargetList(this);
            StartDeadAnimation();
        }
        else
        {
            StartAnimationGetHit();
        }
    }

    public void StartAnimationGetHit()
    {
        mAnimator.SetInteger("ActionNumber", Random.Range(1, 4));
        mAnimator.SetTrigger("GetHit");
        FXGetHitGameObject.SetActive(true);
        StartCoroutine(StopFXEffect(FXGetHitGameObject, 0.8f));
    }

    public void StartDeadAnimation()
    {   

        mAnimator.SetInteger("ActionNumber", Random.Range(1, 5));
        mAnimator.SetTrigger("IsDead");
        mAnimator.SetBool("Dead",true);

        FXGetHitGameObject.SetActive(true);
        FXDeadGameObject.SetActive(true);

        StartCoroutine(StartFXEffect(FXDeadBlood, 1));
        
        StartCoroutine(StopFXEffect(FXGetHitGameObject, 0.8f));
        StartCoroutine(StopFXEffect(FXDeadGameObject,2));

    }

    IEnumerator StopFXEffect(GameObject FXGameObject, float TimeStop)
    {
        yield return new WaitForSeconds(TimeStop);
        FXGameObject.SetActive(false);
    }

    IEnumerator StartFXEffect(GameObject FXGameObject, float TimeStop)
    {
        yield return new WaitForSeconds(TimeStop);
        FXGameObject.SetActive(true);
    }

    public WeaponTypeEnum GetActiveWeaponType()
    {
        WeaponTypeEnum ReturnValue = WeaponTypeEnum.Unarmed;

        foreach (WeaponProperties TekWeaponProperties in ListWeaponProperties)
        {
            if (TekWeaponProperties.gameObject.activeInHierarchy)
            {
                ReturnValue = TekWeaponProperties.WeaponType;
                break;
            }
            
        }

        return ReturnValue;
    }

}

