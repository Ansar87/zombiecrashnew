﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class AnalyticsCustomController : MonoBehaviour
{
    
    public void AddAnalyticEventStartLevel(int LevelId)
    {
        Analytics.CustomEvent("StartLevelGame",
            new Dictionary<string, object>
            {
                {"LevelId",LevelId}
            }
            );
    }

    public void AddAnalyticEventGameOver(int LevelId)
    {
        Analytics.CustomEvent("GameOver",
            new Dictionary<string, object>
            {
                {"LevelId",LevelId}
            }
            );
    }

    public void AddAnalyticEventLevelPass(int LevelId)
    {
        Analytics.CustomEvent("LevelPass",
            new Dictionary<string, object>
            {
                {"LevelId",LevelId}
            }
            );
    }

    public void AddAnalyticEventShowAds()
    {
        Analytics.CustomEvent("ShowAds");
    }

    public void AddAnalyticEventOpenShop()
    {
        Analytics.CustomEvent("OpenShop");
    }

    public void AddAnalyticEventClickButtonBuy()
    {
        Analytics.CustomEvent("ClickButtonBuy");
    }

}
